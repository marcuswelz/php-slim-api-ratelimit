<?php

namespace Mdw\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;
use Mdw\Middleware\RateLimit\ClientIdStrategy;

/**
 * RateLimit Middleware
 *
 * Token Bucket implementation
 *
 */
final class RateLimit
{

    /**
     * The Redis connection string
     *
     * @var string
     */
    private $uri;

    private $requests;
    private $seconds;

    /**
     * Strategy to identify the requesting client
     *
     * @var ClientIdStrategy
     */
    private $clientIdStrategy;

    /**
     * called when a request was rate limited
     *
     * @var callable|null
     */
    private $onRateLimited;

    private $errorPayload = [
        'error' => 'Please throttle your requests.'
    ];

    public function __construct(
        $uri,
        ClientIdStrategy $clientIdStrategy,
        $requests = 10,
        $seconds = 10,
        callable $onRateLimited = null
    ) {
        $this->uri = $uri;
        $this->clientIdStrategy = $clientIdStrategy;
        $this->requests = $requests;
        $this->seconds = $seconds;
        $this->onRateLimited = $onRateLimited;
    }

    public function setErrorPayload($errorPayload)
    {
        $this->errorPayload = $errorPayload;
        return $this;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface|Response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $c = parse_url($this->uri);

        $redis = new \Redis();
        $redis->connect($c['host'], $c['port']);

        $tokens = $redis->rawCommand(
            'RL.REDUCE',
            $this->clientIdStrategy->getClientId($request),
            $this->requests,
            $this->seconds,
            'STRICT'
        );

        if ($tokens > 0) {
            $response = $next($request, $response);
        } else {
            /*
             * Rate limited! Let's tell the client all about it!
             */
            $response = new Response(429);
            if ($this->errorPayload) {
                $response = $response->withJson($this->errorPayload);
            }

            /*
             * If we have a callback, call it, and if it generated a response,
             * we'll use that response instead.
             */
            if (is_callable($this->onRateLimited)) {
                $return = ($this->onRateLimited)($request, $response);
                if ($return instanceof ResponseInterface) {
                    return $return;
                }
            }
        }
        return $response->withHeader('X-RateLimit-Tokens', $tokens);
    }
}
