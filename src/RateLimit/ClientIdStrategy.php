<?php

namespace Mdw\Middleware\RateLimit;

use Psr\Http\Message\ServerRequestInterface;

interface ClientIdStrategy
{
    public function getClientId(ServerRequestInterface $request) : string;
}
