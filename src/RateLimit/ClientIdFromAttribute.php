<?php

namespace Mdw\Middleware\RateLimit;

use Psr\Http\Message\ServerRequestInterface;

final class ClientIdFromAttribute implements ClientIdStrategy
{
    private $attrName = 'apikey';

    public function __construct(string $attrName)
    {
        $this->attrName = $attrName;
    }

    public function getClientId(ServerRequestInterface $request) : string
    {
        return $serverParams = $request->getAttribute($this->attrName);
    }
}
