<?php

namespace Mdw\Middleware\RateLimit;

use Psr\Http\Message\ServerRequestInterface;

final class ClientIdFromServer implements ClientIdStrategy
{
    private $attrName;

    public function __construct(string $attrName = 'REMOTE_ADDR')
    {
        $this->attrName = $attrName;
    }

    public function getClientId(ServerRequestInterface $request) : string
    {
        $serverParams = $request->getServerParams();
        return $serverParams[$this->attrName];
    }
}
