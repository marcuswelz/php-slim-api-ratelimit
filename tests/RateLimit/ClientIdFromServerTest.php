<?php

namespace Mdw\Middleware;

use Slim\Http\Uri;
use Slim\Http\Body;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;
use PHPUnit\Framework\TestCase;
use Mdw\Middleware\RateLimit\ClientIdFromServer;

class ClientIdFromServerTest extends TestCase
{

    /**
     * PSR7 request object.
     *
     * @var Psr\Http\Message\RequestInterface
     */
    protected $request;

    /**
     * PSR7 response object.
     *
     * @var Psr\Http\Message\ResponseInterface
     */
    protected $response;

    protected $headers;

    protected $serverParams;

    protected $body;

    /**
     * Run before each test.
     */
    public function setUp()
    {
        $uri = Uri::createFromString('https://api.example.com:443/api/v1/ping');
        $this->headers = new Headers();
        $this->headers->set('REMOTE_ADDR', '127.0.0.1');
        $this->cookies = [];
        $this->serverParams = Environment::mock()->all();
        $this->body = new Body(fopen('php://temp', 'r+'));
        $this->response = new Response();
        $this->request = new Request('GET', $uri, $this->headers, $this->cookies, $this->serverParams, $this->body);
    }
    public function testCanGetAttributeFromRequest()
    {
        $sut = new ClientIdFromServer();

        $clientId = $sut->getClientId($this->request);
        $this->assertEquals('127.0.0.1', $clientId);
    }
}
